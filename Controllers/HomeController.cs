﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using TeacherManagmentSystem.Models;

namespace TeacherManagmentSystem.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult dashboad()
        {
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("userid"))) return View();
            else return RedirectToAction("Index","Home");
        }
        public IActionResult Privacy()
        {
            return View();
        }
        private Db_Connection db_Connection = new Db_Connection();
        private ErrMessageClass errMessage = new ErrMessageClass();
        public IActionResult LoinAdmin(string _user,string _pass)
        {
            try
            {
                db_Connection.dataAdapter=new MySqlDataAdapter("SELECT * FROM `tblmanage_user` WHERE username='"+_user+"' && password='"+_pass+"'",db_Connection.connection);
                db_Connection.dataTable = new DataTable();
                db_Connection.dataAdapter.Fill(db_Connection.dataTable);
                if(db_Connection.dataTable.Rows.Count==1)
                {
                    errMessage.ErrCode=1;
                    DataRow dr = db_Connection.dataTable.Rows[0];
                    HttpContext.Session.SetString("userid", _user);
                    HttpContext.Session.SetString("Fullname",dr[1].ToString());
                }
                else
                {
                    errMessage.ErrCode=404;
                }
            }
            catch(Exception ex)
            {
                errMessage.ErrCode=ex.HResult;
                errMessage.ErrMessage=ex.Message;
            }
            return Ok(errMessage);
        }
        public IActionResult LogOut()
        {
            HttpContext.Session.SetString("userid","");
            return RedirectToAction("Index", "Home"); 
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
