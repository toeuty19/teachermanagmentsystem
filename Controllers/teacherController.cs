using System;
using Microsoft.AspNetCore.Mvc;
using TeacherManagmentSystem.Models;
using MySql.Data.MySqlClient;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using Microsoft.AspNetCore.Http;

namespace TeacherManagmentSystem.Controllers
{
    public class teacherController : Controller
    {
        public IActionResult ViewTeacher()
        {
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("userid"))) return View();
            else return RedirectToAction("Index","Home");
        }
        Db_Connection db_Connect = new Db_Connection();
        ErrMessageClass errMessage = new ErrMessageClass();
        List<getcombobox> ls_com = new List<getcombobox>();
        List<Teacher_Class> ls_teacher = new List<Teacher_Class>();
        private byte[] img{get;set;}
        private string query{get;set;}
        private ArrayList arrayList = new ArrayList();
        //create function to add data and update
        public void Modify(string _query,Teacher_Class obj)
        {
            if (obj.img != null) img = Convert.FromBase64String(obj.img);
            db_Connect.command = new MySqlCommand(query, db_Connect.connection);
            db_Connect.command.Parameters.AddWithValue("@nkh", obj.name_kh);
            db_Connect.command.Parameters.AddWithValue("@nen", obj.name_en);
            db_Connect.command.Parameters.AddWithValue("@gen", obj.gender);
            db_Connect.command.Parameters.AddWithValue("@dob", obj.dob);
            db_Connect.command.Parameters.AddWithValue("@stid", obj.status_id);
            db_Connect.command.Parameters.AddWithValue("@idcard", obj.id_cad);
            db_Connect.command.Parameters.AddWithValue("@deid", obj.degree_id);
            db_Connect.command.Parameters.AddWithValue("@maid", obj.major_id);
            db_Connect.command.Parameters.AddWithValue("@father", obj.father_name);
            db_Connect.command.Parameters.AddWithValue("@mother", obj.mother_name);
            db_Connect.command.Parameters.AddWithValue("@phone", obj.mobile);
            db_Connect.command.Parameters.AddWithValue("@redate", obj.register_date);
            db_Connect.command.Parameters.AddWithValue("@imgtype", obj.img_type);
            db_Connect.command.Parameters.AddWithValue("@img", img);
            db_Connect.command.ExecuteNonQuery();
        }
        //-------get values of teacher from database---------------------//
        public IActionResult getDataTeacher()
        {
            Teacher_Class obj;
            try{
                query="SELECT `persion_id`,`person_kh`,`persion_en`,`gender`,`per_dob`,`status_id`,`nation_id_card`,`degree_kh`,`major_kh`,`father_name`,`mother_name`, `phone`,`register_date`,`image_type`,`image`,tbl_persion.degree_id,tbl_persion.major_id FROM tbl_persion,tbl_degree,tbl_major WHERE tbl_persion.degree_id=tbl_degree.degree_id AND tbl_persion.major_id=tbl_major.major_id ORDER BY `tbl_persion`.persion_id DESC";
                db_Connect.dataAdapter = new MySqlDataAdapter(query,db_Connect.connection);
                db_Connect.dataTable = new DataTable();
                db_Connect.dataAdapter.Fill(db_Connect.dataTable);
                if(db_Connect.dataTable.Rows.Count>0)
                {
                    foreach(DataRow dr in db_Connect.dataTable.Rows)
                    {
                        obj=new Teacher_Class();
                        obj.teacher_id=int.Parse(dr[0].ToString());
                        obj.name_kh = dr[1].ToString();
                        obj.name_en = dr[2].ToString();
                        obj.gender = dr[3].ToString();
                        DateTime date = Convert.ToDateTime(dr[4].ToString());
                        obj.dob_str = date.ToString("yyyy-MM-dd");
                        obj.status_id = int.Parse(dr[5].ToString());
                        obj.id_cad = dr[6].ToString();
                        obj.degree_kh = dr[7].ToString();
                        obj.major_kh = dr[8].ToString();
                        obj.father_name = dr[9].ToString();
                        obj.mother_name = dr[10].ToString();
                        obj.mobile = dr[11].ToString();
                        DateTime R_date = Convert.ToDateTime(dr[12].ToString());
                        obj.re_date_str = R_date.ToString("yyyy-MM-dd");
                        obj.img_type = dr[13].ToString();
                        img = (byte[])dr[14];
                        obj.img =obj.img_type+","+ Convert.ToBase64String(img);
                        obj.degree_id=int.Parse(dr[15].ToString());
                        obj.major_id=int.Parse(dr[16].ToString());
                        ls_teacher.Add(obj);
                    }
                }

            }catch(Exception ex)
            {
                errMessage.ErrCode=ex.HResult;
                errMessage.ErrMessage=ex.Message;
            }
            arrayList.Add(ls_teacher);
            arrayList.Add(errMessage);
            return Ok(arrayList);
        }
        //--save values
        [HttpPost]
        public IActionResult PostTeacher(Teacher_Class get_teacher_obj)
        {
            query="INSERT INTO tbl_persion(person_kh, persion_en, gender, per_dob, status_id, nation_id_card, degree_id, major_id, father_name, mother_name, phone, register_date, image_type, image) VALUES(@nkh,@nen,@gen,@dob,@stid,@idcard,@deid,@maid,@father,@mother,@phone,@redate,@imgtype,@img)";
            try
            {
                if (errMessage.ErrCode == 0)
                {
                    Modify(query,get_teacher_obj);
                    errMessage.ErrCode = 1;
                    errMessage.ErrMessage = "Informaton was saved.";
                }
                else 
                {
                    errMessage.ErrCode = 404; 
                    errMessage.ErrMessage = "Internet not connected.";
                }
                     
            }
            catch (Exception ex)
            {
                errMessage.ErrCode = ex.HResult;
                errMessage.ErrMessage = ex.Message;
            }
            return Ok(errMessage);
        }
        //--update values
        [HttpPost]
        public IActionResult UpdateTeacher(Teacher_Class obj_teacher)
        {
            query = "UPDATE `tbl_persion` SET person_kh=@nkh, persion_en=@nen, gender=@gen, per_dob=@dob, status_id=@stid, nation_id_card=@idcard, degree_id=@deid, major_id=@maid, father_name=@father, mother_name=@mother, phone=@phone, register_date=@redate, image_type=@imgtype, image=@img WHERE `persion_id`="+obj_teacher.teacher_id;
            try
            {
                if (errMessage.ErrCode == 0)
                {
                    Modify(query, obj_teacher);
                    errMessage.ErrCode = 1;
                    errMessage.ErrMessage = "Informaton was Updated.";
                }
                else
                {
                    errMessage.ErrCode = 404;
                    errMessage.ErrMessage = "Internet not connected.";
                }

            }
            catch (Exception ex)
            {
                errMessage.ErrCode = ex.HResult;
                errMessage.ErrMessage = ex.Message;
            }
            return Ok(errMessage);
        }
        public IActionResult DeleteTeacher(int pid)
        {
            try
            {
                query = "DELETE FROM tbl_persion WHERE persion_id="+pid;
                db_Connect.command = new MySqlCommand(query,db_Connect.connection);
                db_Connect.command.ExecuteNonQuery();
                errMessage.ErrCode=1;
                errMessage.ErrMessage="Successfully.";

            }
            catch(Exception ex)
            {
                errMessage.ErrCode=ex.HResult;
                errMessage.ErrMessage=ex.Message;
            }
            return Ok(errMessage);
        }
        //combobox status
        [HttpGet]
        public IActionResult status()
        {
            try
            {
                query = "SELECT * FROM tbl_persionstatus";
                GetVialueToComboBox(query);
            }
            catch(Exception ex)
            {
                throw(ex);
            }
            return Ok(ls_com);
        }
        //combobox degree
        [HttpGet]
        public IActionResult degree()
        {
            try{
                query = "SELECT * FROM tbl_degree";
                GetVialueToComboBox(query);
            }
            catch(Exception ex)
            {
                throw(ex);
            }
            return Ok(ls_com);
        }
        //combobox major
        [HttpGet]
        public IActionResult major()
        {
            try{
                query = "SELECT * FROM tbl_major";
                GetVialueToComboBox(query);
            }
            catch(Exception ex){throw(ex);}
            return Ok(ls_com);
        }
        //create function to get values from database
        public void GetVialueToComboBox(string query)
        {
            try
            {
                db_Connect = new Db_Connection();
                db_Connect.dataAdapter = new MySqlDataAdapter(query, db_Connect.connection);
                db_Connect.dataTable = new DataTable();
                db_Connect.dataAdapter.Fill(db_Connect.dataTable);
                if (db_Connect.dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in db_Connect.dataTable.Rows)
                    {
                        getcombobox com = new getcombobox();
                        com.cid = int.Parse(dr[0].ToString());
                        com.cname = dr[1].ToString();
                        ls_com.Add(com);
                    }
                }
            }
            catch(Exception ex)
            {
                throw(ex);
            }
        }
    }
}