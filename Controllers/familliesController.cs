using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using TeacherManagmentSystem.Models;

namespace TeacherManagmentSystem.Controllers
{
    public class familliesController:Controller
    {
        private Db_Connection db_Connection = new Db_Connection();
        private ErrMessageClass msg = new ErrMessageClass();
        private List<ErrMessageClass> ls_msg=new List<ErrMessageClass>();
        private string _query{get;set;}
        private List<getcombobox> _list=new List<getcombobox>();
        private getcomboList obj_list=new getcomboList();
        private List<family_class> ls_fam = new List<family_class>();
        private ArrayList arrayList = new ArrayList();
        private family_class obj;
        public IActionResult viewFamilies()
        {
            if(!string.IsNullOrEmpty(HttpContext.Session.GetString("userid"))) return View();
            else return RedirectToAction("Index","Home");
        }
        public IActionResult postFamily(family_class get_obj)
        {
            try
            {
                _query="INSERT INTO tbl_family(persion_id, fam_kh, fam_en, fam_contact, occ_id, child_num, son, dauther) VALUES(@pid,@fnamekh,@fnameEn,@contact,@occId,@childNum,@sonNum,@doutNum)";
                InsertRecord(_query,get_obj);
                msg.ErrCode=1;
                msg.ErrMessage="One record has been saved to database.";
            }
            catch(Exception ex)
            {
                msg.ErrMessage=ex.Message;
                msg.ErrCode=ex.HResult;
            }
            return Ok(msg);
        }
        public IActionResult UpdateFamilies(family_class get_obj)
        {
            try
            {
                _query="UPDATE tbl_family SET persion_id=@pid, fam_kh=@fnamekh, fam_en=@fnameEn, fam_contact=@contact, occ_id=@occId, child_num=@childNum, son=@sonNum, dauther=@doutNum WHERE fam_id="+get_obj.famid;
                InsertRecord(_query,get_obj);
                msg.ErrMessage="The valuse has been updated...";
                msg.ErrCode=1;
            }
            catch(Exception ex)
            {
                msg.ErrCode=ex.HResult;
                msg.ErrMessage=ex.Message;
            }
            return Ok(msg);
        }
        public IActionResult deleteFamilise(int famId)
        {
            try
            {
                _query="DELETE FROM tbl_family  WHERE fam_id="+famId;
                db_Connection.command = new MySqlCommand(_query,db_Connection.connection);
                db_Connection.command.ExecuteNonQuery();
                msg.ErrCode=1;
                msg.ErrMessage="The one row has been delete successfully.";
            }
            catch(Exception ex)
            {
                msg.ErrCode=ex.HResult;
                msg.ErrMessage=ex.Message;
            }
            return Ok(msg);
        }
        public IActionResult GetOccupation()
        {
            try
            {
                _query = "SELECT * FROM `tbl_occupations`";
                getCombo(_query);
            }
            catch (Exception ex)
            {
                msg.ErrCode = ex.HResult;
                msg.ErrMessage = ex.Message;
            }
            ls_msg.Add(msg);
            obj_list.ls_com = _list;
            obj_list.ls_errMsg = ls_msg;
            return Ok(obj_list);
        }
        public IActionResult GetTeacherValue()
        {
            try
            {
                _query = "SELECT persion_id,person_kh From tbl_persion";
                getCombo(_query);
            }
            catch(Exception ex){
                msg.ErrCode=ex.HResult;
                msg.ErrMessage=ex.Message;
            }
            ls_msg.Add(msg);
            obj_list.ls_com=_list;
            obj_list.ls_errMsg=ls_msg;
            return Ok(obj_list);
        }
        //SELECT `fam_id`,`person_kh`,`fam_kh`,`fam_en`,`fam_contact`,occ_kh,`child_num`,`son`,`dauther` FROM `tbl_family`,tbl_persion,tbl_occupations WHERE tbl_family.persion_id=tbl_persion.persion_id AND tbl_family.occ_id=tbl_occupations.occ_id
        public IActionResult display_record()
        {
            _query="SELECT `fam_id`,`person_kh`,`fam_kh`,`fam_en`,`fam_contact`,occ_kh,`child_num`,`son`,`dauther` FROM `tbl_family`,tbl_persion,tbl_occupations WHERE tbl_family.persion_id=tbl_persion.persion_id AND tbl_family.occ_id=tbl_occupations.occ_id";
            DesplayRecord(_query);
            return Ok(arrayList);
        }
        //get record display on text field
        [HttpPost]
        public IActionResult getRecord(int get_id)
        {
            _query="SELECT * FROM `tbl_family` WHERE `fam_id`="+get_id;
            Get_Record(_query);
            return Ok(obj);
        }
        [HttpGet]
        
        //create function insert record to database
        private void InsertRecord(string query,family_class get_fam)
        {
            try
            {
                db_Connection.command = new MySqlCommand(query, db_Connection.connection);
                db_Connection.command.Parameters.AddWithValue("@pid", get_fam.pid);
                db_Connection.command.Parameters.AddWithValue("@fnamekh", get_fam.fnamekh);
                db_Connection.command.Parameters.AddWithValue("@fnameEn", get_fam.fnameEn);
                db_Connection.command.Parameters.AddWithValue("@contact", get_fam.contact);
                db_Connection.command.Parameters.AddWithValue("@occId", get_fam.occId);
                db_Connection.command.Parameters.AddWithValue("@childNum", get_fam.childNum);
                db_Connection.command.Parameters.AddWithValue("@sonNum", get_fam.sonNum);
                db_Connection.command.Parameters.AddWithValue("@doutNum", get_fam.doutNum);
                db_Connection.command.ExecuteNonQuery();
            }
            catch(Exception ex) 
            {
                msg.ErrCode=ex.HResult;
                msg.ErrMessage=ex.Message;
            }
        }
        //create function display all data from database
        private void DesplayRecord(string query)//we can use like this
        {
            try
            {
                db_Connection.dataAdapter = new MySqlDataAdapter(query,db_Connection.connection);
                db_Connection.dataTable = new DataTable();
                db_Connection.dataAdapter.Fill(db_Connection.dataTable);
                foreach(DataRow dr in db_Connection.dataTable.Rows)
                {
                    family_class fam = new family_class();
                    fam.famid=int.Parse(dr[0].ToString());
                    fam.pname=dr[1].ToString();
                    fam.fnamekh=dr[2].ToString();
                    fam.fnameEn=dr[3].ToString();
                    fam.contact=dr[4].ToString();
                    //fam.occId=int.Parse(dr[5].ToString());
                    fam.ocname=dr[5].ToString();
                    fam.childNum=int.Parse(dr[6].ToString());
                    fam.sonNum=int.Parse(dr[7].ToString());
                    fam.doutNum=int.Parse(dr[8].ToString());
                    ls_fam.Add(fam);   
                }
            }
            catch (Exception ex)
            {
                msg.ErrCode=ex.HResult;
                msg.ErrMessage=ex.Message;
            }
            arrayList.Add(ls_fam);
            arrayList.Add(msg);
        }
        private void getCombo(string query)
        {
            db_Connection.dataAdapter = new MySqlDataAdapter(query, db_Connection.connection);
            db_Connection.dataTable = new DataTable();
            db_Connection.dataAdapter.Fill(db_Connection.dataTable);
            if(db_Connection.dataTable.Rows.Count>0)
            {
                foreach(DataRow dr in db_Connection.dataTable.Rows)
                {
                    getcombobox obj = new getcombobox();
                    obj.cid=int.Parse(dr[0].ToString());
                    obj.cname=dr[1].ToString();
                    _list.Add(obj);
                }
            }
        }
        //create function display record on text field
        public void Get_Record(string query)
        {
            db_Connection.command = new MySqlCommand(query,db_Connection.connection);
            MySqlDataReader dr = db_Connection.command.ExecuteReader();
            while(dr.Read())
            {
                obj = new family_class();
                obj.famid = int.Parse(dr[0].ToString());
                obj.pid=int.Parse(dr[1].ToString());
                obj.fnamekh=dr[2].ToString();
                obj.fnameEn=dr[3].ToString();
                obj.contact=dr[4].ToString();
                obj.occId=int.Parse(dr[5].ToString());
                obj.childNum=int.Parse(dr[6].ToString());
                obj.sonNum=int.Parse(dr[7].ToString());
                obj.doutNum=int.Parse(dr[8].ToString());
            }
        }
    }
}