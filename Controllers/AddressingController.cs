using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using TeacherManagmentSystem.Models;

namespace TeacherManagmentSystem.Controllers
{
    public class AddressingController : Controller
    {
        getcombobox com ;
        List<getcombobox> list_combo = new List<getcombobox>();
        Db_Connection db_connect = new Db_Connection();
        ErrMessageClass msg = new ErrMessageClass();
        List<ErrMessageClass> ls_errsmg;
        AddressClass address;
        List<AddressClass> ls_address = new List<AddressClass>();
        ArrayList arrayList = new ArrayList();
        string _query;
        public IActionResult viewAddressing()
        {
            return View();
        }
        public IActionResult Insert_user(AddressClass get_obj)
        {
            try
            {
                _query = "INSERT INTO tbl_address(persion_id, home_num, street_num, province_id, district_id, commune_id, village_id) VALUES (@pid,@home,@street,@proId,@disId,@comId,@vilId)";
                Modify(_query, get_obj);
            }
            catch(Exception e) 
            {
                msg.ErrCode=e.HResult;
                msg.ErrMessage=e.Message;
            }
            return Ok(msg);
        }
        public IActionResult Update_user(AddressClass get_objs)
        {
            try
            {
                //@pid,@home,@street,@proId,@disId,@comId,@vilId
                _query = "UPDATE tbl_address SET persion_id=@pid, home_num=@home, street_num=@street, province_id=@proId, district_id=@disId, commune_id=@comId, village_id=@vilId WHERE add_id="+get_objs.addId;
                Modify(_query, get_objs);
            }
            catch (Exception e)
            {
                msg.ErrCode = e.HResult;
                msg.ErrMessage = e.Message;
            }
            return Ok(msg);
        }
        public IActionResult deleteUser(int getuserId)
        {
            try
            {
                _query="DELETE FROM tbl_address WHERE add_id=@userid";
                db_connect.command = new MySqlCommand(_query , db_connect.connection);
                db_connect.command.Parameters.AddWithValue("@userid",getuserId);
                db_connect.command.ExecuteNonQuery();
            }
            catch(Exception e)
            {
                msg.ErrCode=e.HResult;
                msg.ErrMessage=e.Message;
            }
            return Ok(msg);
        }
        //call data from database 

        public IActionResult display_address()
        {
            try
            {
                _query="SELECT add_id,person_kh,home_num,street_num,province_kh_name,district_namekh,commune_namekh,village_namekh FROM tbl_persion,provinces,districts,communes,villages,tbl_address WHERE tbl_address.persion_id=tbl_persion.persion_id AND tbl_address.province_id=provinces.province_id AND tbl_address.district_id=districts.dis_id AND tbl_address.commune_id=communes.com_id AND tbl_address.village_id=villages.vill_id";
                db_connect.dataAdapter = new MySqlDataAdapter(_query,db_connect.connection);
                db_connect.dataTable = new DataTable();
                db_connect.dataAdapter.Fill(db_connect.dataTable);
                foreach(DataRow row in db_connect.dataTable.Rows)
                {
                    address = new AddressClass();
                    address.addId = int.Parse(row[0].ToString());
                    address.pname=row[1].ToString();
                    address.homeNum=row[2].ToString();
                    address.streetNum=row[3].ToString();
                    address.proname=row[4].ToString();
                    address.disname=row[5].ToString();
                    address.cname=row[6].ToString();
                    address.vname=row[7].ToString();
                    ls_address.Add(address);
                }
            }
            catch(Exception e)
            {
                msg.ErrCode=e.HResult;
                msg.ErrMessage=e.Message;
                
            }
            ls_errsmg = new List<ErrMessageClass>();
            ls_errsmg.Add(msg);
            arrayList.Add(ls_address);
            arrayList.Add(ls_errsmg);
            return Ok(arrayList);
        }
        /// <summary>
        /// ...
        /// </summary>
        /// <returns></returns>
        /// get record user
        public IActionResult GetRowUser(int _getuserid)
        {
            try
            {
                _query = "SELECT persion_id,home_num,street_num,province_id,district_id,commune_id,village_id FROM tbl_address WHERE add_id ="+_getuserid;
                db_connect.dataAdapter = new MySqlDataAdapter(_query,db_connect.connection);
                db_connect.dataTable = new DataTable();
                db_connect.dataAdapter.Fill(db_connect.dataTable);
                if(db_connect.dataTable.Rows.Count>0)
                {
                    foreach(DataRow row in db_connect.dataTable.Rows)
                    {
                        address = new AddressClass();
                        address.pid = int.Parse(row[0].ToString());
                        address.homeNum = row[1].ToString();
                        address.streetNum = row[2].ToString();
                        address.proId = int.Parse(row[3].ToString());
                        address.disId = int.Parse(row[4].ToString());
                        address.comId = int.Parse(row[5].ToString());
                        address.vilageId = int.Parse(row[6].ToString());
                        ls_address.Add(address);
                    }

                }
            }
            catch(Exception e){throw(e);}
            return Ok(ls_address);
        }
        public IActionResult teacher()
        {
            try
            {
                _query = "SELECT * FROM tbl_persion";
                combobox(_query);
            }
            catch (Exception e)
            {
                throw (e);
            }
            return Ok(list_combo);
        }
        public IActionResult province()
        {
            try
            {
                _query = "SELECT province_id,province_kh_name FROM provinces";
                combobox(_query);
            }
            catch(Exception e)
            {
                throw(e);
            }
            return Ok(list_combo);
        }
        public IActionResult district(int _getid)
        {
            try
            {
                _query="SELECT dis_id,district_namekh FROM districts WHERE pro_id="+_getid;
                combobox(_query);
            }
            catch(Exception e)
            {
                throw(e);
            }
            return Ok(list_combo);
        }
        public IActionResult communue(int _getid)
        {
            try
            {
                _query = "SELECT com_id,commune_namekh FROM communes WHERE district_id=" + _getid;
                combobox(_query);
            }
            catch (Exception e)
            {
                throw (e);
            }
            return Ok(list_combo);
        }
        public IActionResult Village(int _getid)
        {
            try
            {
                _query = "SELECT vill_id,village_namekh FROM villages WHERE commune_id=" + _getid;
                combobox(_query);
            }
            catch (Exception e)
            {
                throw (e);
            }
            return Ok(list_combo);
        }
        //-------------------update combobox-------------------------//
        public IActionResult udistrict(int _getid)
        {
            try
            {
                _query = "SELECT dis_id,district_namekh FROM districts WHERE pro_id" ;
                combobox(_query);
            }
            catch (Exception e)
            {
                throw (e);
            }
            return Ok(list_combo);
        }
        public IActionResult ucommunue(int _getid)
        {
            try
            {
                _query = "SELECT com_id,commune_namekh FROM communes WHERE district_id";
                combobox(_query);
            }
            catch (Exception e)
            {
                throw (e);
            }
            return Ok(list_combo);
        }
        public IActionResult uVillage(int _getid)
        {
            try
            {
                _query = "SELECT vill_id,village_namekh FROM villages WHERE commune_id";
                combobox(_query);
            }
            catch (Exception e)
            {
                throw (e);
            }
            return Ok(list_combo);
        }
        //---------------------section function-------------------///
        //set commbo function
        public void combobox(string SetQuery)
        {
            try
            {
                db_connect.dataAdapter = new MySqlDataAdapter(SetQuery,db_connect.connection);
                db_connect.dataTable = new DataTable();
                db_connect.dataAdapter.Fill(db_connect.dataTable);
                foreach(DataRow row in db_connect.dataTable.Rows)
                {
                    com = new getcombobox();
                    com.cid = int.Parse(row[0].ToString());
                    com.cname = row[1].ToString();
                    list_combo.Add(com);
                }
            }
            catch(Exception e)
            {
                throw(e);
            }
        }
        public void Modify(string query,AddressClass get_obj)
        {
            db_connect.command = new MySqlCommand(query, db_connect.connection);
            db_connect.command.Parameters.AddWithValue("@pid", get_obj.pid);
            db_connect.command.Parameters.AddWithValue("@home", get_obj.homeNum);
            db_connect.command.Parameters.AddWithValue("@street", get_obj.streetNum);
            db_connect.command.Parameters.AddWithValue("@proId", get_obj.proId);
            db_connect.command.Parameters.AddWithValue("@disId", get_obj.disId);
            db_connect.command.Parameters.AddWithValue("@comId", get_obj.comId);
            db_connect.command.Parameters.AddWithValue("@vilId", get_obj.vilageId);
            db_connect.command.ExecuteNonQuery();
        }
    }
}