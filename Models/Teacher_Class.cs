using System;
using System.Collections.Generic;

namespace TeacherManagmentSystem.Models
{
    public class Teacher_Class
    {
        public int teacher_id{get;set;}
        public string name_kh{get;set;}
        public string name_en{get;set;}
        public string gender{get;set;}
        public DateTime dob{get;set;}
        public string dob_str{get;set;}
        public int status_id{get;set;}
        public string id_cad{get;set;}
        public int degree_id{get;set;}
        public string degree_kh{get;set;}
        public int major_id{get;set;}
        public string major_kh{get;set;}
        public string father_name{get;set;}
        public string mother_name{get;set;}
        public string mobile{get;set;}
        public DateTime register_date{get;set;}
        public string re_date_str{get;set;}
        public string img_type{get;set;}
        public string img{get;set;}
        public ErrMessageClass err_obj_list{get;set;}
    }
}