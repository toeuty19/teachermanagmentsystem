namespace TeacherManagmentSystem.Models
{
    public class family_class
    {
        public int famid { get; set; }
        public int pid { get; set; }
        public string pname{get;set;}
        public string fnamekh { get; set; }
        public string fnameEn { get; set; }
        public string contact { get; set; }
        public int occId { get; set; }
        public string ocname { get; set; }
        public int childNum { get; set; }
        public  int sonNum { get; set; }
        public int doutNum { get; set; }
    }
}