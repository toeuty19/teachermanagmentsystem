namespace TeacherManagmentSystem.Models
{
    public class ErrMessageClass
    {
        public int ErrCode { get; set; }
        public string ErrMessage { get; set; }
    }
}