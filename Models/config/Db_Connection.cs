using System;
using System.Data;
using MySql.Data.MySqlClient;
namespace TeacherManagmentSystem.Models
{
    public class Db_Connection
    {
        public MySqlConnection connection{get;set;}
        public MySqlCommand command{get;set;}
        public MySqlDataAdapter dataAdapter{get;set;}
        public DataTable dataTable{get;set;}
        private ErrMessageClass errMessage=new ErrMessageClass();
        public Db_Connection()
        {
            MyDb_Connection();
        }
        private MySqlConnection MyDb_Connection()
        {
            try
            {
                string _getStringConnection = Db_ConnectionString._setStringConnection;
                connection = new MySqlConnection(_getStringConnection);
                if(connection.State==ConnectionState.Closed) connection.Open();

                if(connection.State==ConnectionState.Open) {errMessage.ErrCode=0;errMessage.ErrMessage="Connected.";}
                else {errMessage.ErrCode=404;errMessage.ErrMessage="Not Connected.";}
            }
            catch(Exception ex)
            {
                errMessage.ErrCode=ex.HResult;
                errMessage.ErrMessage=ex.Message;
            }
            return connection;
        }
    }
 
}