using System;
namespace TeacherManagmentSystem.Models
{
    public class AddressClass
    {
        public int addId { get; set; }
        public int pid { get; set; }
        public string pname { get; set; }
        public string homeNum { get; set; }
        public string streetNum { get; set; }
        public int proId { get; set; }
        public string proname { get; set; }
        public int disId{get;set;}
        public string disname { get; set; }
        public int comId { get; set; }
        public string cname { get; set; }
        public int vilageId { get; set; }
        public string vname { get; set; }
    }
}